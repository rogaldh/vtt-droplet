SHELL := /usr/bin/env bash

CDIR ?= $(shell pwd)
CLIENT_DIR := client
NODE_VER := $(shell cat .nvmrc)
VOLUME_DIR := volume

setup:
	@echo " 🛠 Setting environment up..."; \
	./setup.sh ${NODE_VER};

foundry:
	@echo " 📦 Fetching the app...";
	@echo " 🔗 Enter Foundry VTT build link to download: "; \
	read BUILD_ADDR; \
	curl -o FoundryVTT.zip $${BUILD_ADDR}
	unzip FoundryVTT.zip -d ${CLIENT_DIR};\
	mkdir -p ${VOLUME_DIR};
	@echo " ❗️Run this at the terminal on a fresh start:";
	@echo " . ~/.nvm/nvm.sh";

launch:
	@echo " 🚀 Launching FoundryVTT..."; \
	pm2 start \
		"${CDIR}/${CLIENT_DIR}/resources/app/main.js" -- --dataPath="${CDIR}/${VOLUME_DIR}";
