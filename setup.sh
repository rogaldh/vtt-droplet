#!/usr/bin/env bash

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
. /root/.nvm/nvm.sh
nvm install $1
npm i
npx yarn global add pm2
