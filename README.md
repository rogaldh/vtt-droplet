# @vtt-kit/droplet

This tool allows easing the FoundryVTT launch on a virtual server.

## Launch on Ubuntu

```bash
apt-get install make unzip
git clone <%REPO_LINK%> droplet
make -C droplet setup foundry

. ~/.nvm/nvm.sh
# You have to make `nvm` visible for the current sheel via ^ launch or by shell' relaunch.
make -C droplet launch
```

To run already installed VTT use just `launch`, e.g. `make -C droplet launch`.

## Customize directory

Tool will use `/root` dir by default. Use `CDIR` variable to change this, e.g. `CDIR=$(pwd) make ..`

## License
MIT
